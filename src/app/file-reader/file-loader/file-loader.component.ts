import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-file-loader',
  templateUrl: './file-loader.component.html',
  styleUrls: ['./file-loader.component.css']
})
export class FileLoaderComponent implements OnInit {
  @Input()
  file: File = null;

  @Output()
  fileLoaded: EventEmitter<any> = new EventEmitter();

  loadingProgress = 0;

  ngOnInit(): void {
    if (this.file) {
      this.readFile(this.file);
    }
  }

  readFile(file: Blob) {
    const fileReader: FileReader = new FileReader();
    const self = this;
    fileReader.readAsText(file, 'UTF-8');
    fileReader.onprogress = function (data) {
      if (data.lengthComputable) {
        self.loadingProgress = ((data.loaded / data.total) * 100);
      }
    };
    fileReader.onload = function (event) {
      self.fileLoaded.next(event.target);
    };
  }
}
