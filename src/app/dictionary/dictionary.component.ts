import {Component, ElementRef, ViewChild} from '@angular/core';
import {saveAs} from 'file-saver/dist/FileSaver';
import posTagger from 'wink-pos-tagger';
import {Tag} from 'en-pos';
import {lexicon} from 'en-lexicon';
import compendium from 'compendium-js';

const enum sortType {
  SORT_TYPE_WORD,
  SORT_TYPE_COUNT
}

const enum DialogOpenMode {
  OPEN_MODE_WORD,
  OPEN_MODE_TAG,
  OPEN_MODE_ADD_TAG_DEFINITION,
  OPEN_MODE_CANON_FORM,
  OPEN_MODE_CANON_FORM_TAGS,
  OPEN_MODE_CREATE_WORD
}

const baseForms: string[] = ['VB', 'RB', 'NNP', 'NN', 'JJ', 'RB', 'RP', 'UH', 'PRP', 'TO', 'CC', 'CD', 'DT', 'MD',
  'WDT', 'WP', 'WRB', 'UH'];

interface TaggedWord {
  value: string;
  tag: string;
  normal: string;
  pos: string;
  lemma?: string;
}

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.less']
})

export class DictionaryComponent {
  wordMap: Map<string, { count: number, tags: string, canonForm: string }> = new Map();
  canonFormMap: Map<string, string> = new Map();
  wordArray = [];
  dialogOpenMode: DialogOpenMode;
  annotatedText = '';
  safeAnnotatedText = '';
  annotatedTextIsEdit = false;
  annotatedTextWithError = false;
  isTooltipVisible = false;

  private tagsDefinition = new Object({
    'CC': 'союз(and,but,or)',
    'CD': 'количественное числительное',
    'DT': 'Детерминанта(a, the, every)',
    'EX': 'экзистенциальное there (there are...)',
    'IN': 'Предлог',
    'JJ': 'Прилагательное',
    'JJR': 'Прилагательное (сравнительное)',
    'JJS': 'Прилагательное (превосходное),',
    'MD': 'Модальный глагол',
    'NN': 'Существительное',
    'NNS': 'Существительное множественное',
    'NNP': 'Имя собственное (ед.)',
    'NNPS': 'Имя собственное (множ.)',
    'PDT': 'Predeterminer(both, a lot of)',
    'POS': 'Притяжное окончание (апостроф)',
    'PRP': 'Личное местоимение',
    'PRP$': 'Притяжательное местоимение',
    'RB': 'Наречие',
    'RBR': 'Наречие сравнительное',
    'RBS': 'Наречие превосходное',
    'RP': 'Частица',
    'TO': 'To',
    'UH': 'Междометие',
    'VB': 'Глагол (base form)',
    'VBD': 'Глагол (прошедш.)',
    'VBG': 'Герундий/причастие',
    'VBN': 'Причастие (прошедш.)',
    'VBP': 'Глагол ед.ч. не 3 лицо',
    'VBZ': 'Глагол ед.ч. 3 лицо',
    'WDT': 'Wh-детерминанта (What, Which, Whose)',
    'WP': 'Wh-существительное(Who, Which, What)',
    'WP$': 'Wh-притяжательное местоимение(Whose)',
    'WRB': 'Wh-наречие(When, Where, How, Why)',
    '.': 'Последний символ пунктуации в предложение',
    ',': 'НЕ последний символ пунктуации в предложении'
  });
  tagsDefinitionArray = Object.entries(this.tagsDefinition);
  statisticsTag: Map<string, number> = new Map();
  statisticsTagArray = [];
  statisticsWordTag: Map<string, number> = new Map();
  statisticsWordTagArray = [];
  statisticsTagTagPair: Map<string, number> = new Map();
  statisticsTagTagPairArray = [];
  tagSearch = '';
  wordSearch = '';
  tagNameUp = false;
  tagCountUp = false;
  wordTagTagUp = false;
  wordTagWordUp = false;
  wordTagCountUp = false;
  tagTagTagUp = false;
  tagTagCountUp = false;
  JSON = JSON;
  wordUp = false;
  countUp = false;
  editWord = '';
  newWord = '';
  private last: sortType;
  private tagger;
  private wordForTag: string;
  private enTag;

  @ViewChild('dialog')
  dialogWindow: ElementRef;
  @ViewChild('annotatedTextComponent')
  annotatedTextComponent: ElementRef;
  @ViewChild('dictionaryTableElement')
  dictionaryTable: ElementRef;
  @ViewChild('tagTableElement')
  tagTable: ElementRef;
  @ViewChild('wordTagTableElement')
  wordTagTable: ElementRef;
  @ViewChild('tagTagTableElement')
  tagTagTable: ElementRef;

  constructor() {
    this.tagger = posTagger();
    this.enTag = Tag;
    this.tagsDefinitionArray.forEach((tag, index) => {
      this.statisticsTag.set(tag[0], 0);
      this.tagsDefinitionArray.forEach((tag2, index2) => {
        if (index2 >= index) {
          const tags: string = JSON.stringify([tag2[0], tag[0]].sort());
          this.statisticsTagTagPair.set(tags, 0);
        }
      });
    });
    this.statisticsTagArray = Array.from(this.statisticsTag);
    this.statisticsTagTagPairArray = Array.from(this.statisticsTagTagPair);
  }

  onFileParsed(text: string) {
    const taggedArray = this.tagger.tagSentence(text);
    const wordArray = [];
    let wordsFromQuotes = [];
    this.annotatedText = '';
    taggedArray.forEach((taggedWord: TaggedWord) => {
      if (taggedWord.tag === 'word') {
        this.addWordToMap(taggedWord.normal, 1);
      }
      if (taggedWord.tag === 'quoted_phrase') {
        const phrase = taggedWord.normal.slice(1, -1);
        wordsFromQuotes = wordsFromQuotes.concat(this.tagger.tagSentence(phrase));
      } else {
        wordArray.push(taggedWord.value);
      }
    });
    wordsFromQuotes.forEach((taggedWord: TaggedWord) => {
      if (taggedWord.tag === 'word') {
        this.addWordToMap(taggedWord.normal, 1);
      }
      wordArray.push(taggedWord.value);
    });
    const posTags = new Tag(wordArray).initial().smooth();
    let prevTag = '';
    posTags.tokens.forEach((token, index) => {
      this.annotatedText += token + '_' + posTags.tags[index] + ' ';
      if (this.statisticsTag.has(posTags.tags[index])) {
        const initialCount = this.statisticsTag.get(posTags.tags[index]);
        this.statisticsTag.set(posTags.tags[index], initialCount + 1);
      } else {
        this.statisticsTag.set(posTags.tags[index], 1);
      }
      const wordAndTag = JSON.stringify({word: token.toLowerCase(), tag: posTags.tags[index]});
      if (this.statisticsWordTag.has(wordAndTag)) {
        const initialCount = this.statisticsWordTag.get(wordAndTag);
        this.statisticsWordTag.set(wordAndTag, initialCount + 1);
      } else {
        this.statisticsWordTag.set(wordAndTag, 1);
      }
      if (prevTag) {
        const tags: string = JSON.stringify([prevTag, posTags.tags[index]].sort());
        if (this.statisticsTagTagPair.has(tags)) {
          const initialCount = this.statisticsTagTagPair.get(tags);
          this.statisticsTagTagPair.set(tags, initialCount + 1);
        } else {
          this.statisticsTagTagPair.set(tags, 1);
        }
      }
      prevTag = posTags.tags[index];
    });
    this.wordArray = Array.from(this.wordMap);
    this.statisticsTagArray = Array.from(this.statisticsTag);
    this.statisticsWordTagArray = Array.from(this.statisticsWordTag);
    this.statisticsTagTagPairArray = Array.from(this.statisticsTagTagPair);
    this.last = sortType.SORT_TYPE_WORD;
    this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
  }

  onAnnotatedFileParsed(text: string) {
    const possibleWords = text.split(' ');
    let prevTag = '';
    possibleWords.forEach((possibleWord: string) => {
      const taggedWordArray = possibleWord.split('_');
      if (taggedWordArray.length === 2) {
        if (this.tagger.tagSentence(taggedWordArray[0])[0].tag === 'word') {
          this.addWordToMap(
            taggedWordArray[0],
            1,
            taggedWordArray[1]);
        }
        if (this.statisticsTag.has(taggedWordArray[1])) {
          const initialCount = this.statisticsTag.get(taggedWordArray[1]);
          this.statisticsTag.set(taggedWordArray[1], initialCount + 1);
        } else {
          this.statisticsTag.set(taggedWordArray[1], 1);
        }
        const wordAndTag = JSON.stringify({word: taggedWordArray[0], tag: taggedWordArray[1]});
        if (this.statisticsWordTag.has(wordAndTag)) {
          const initialCount = this.statisticsWordTag.get(wordAndTag);
          this.statisticsWordTag.set(wordAndTag, initialCount + 1);
        } else {
          this.statisticsWordTag.set(wordAndTag, 1);
        }
        if (prevTag) {
          const tags: string = JSON.stringify([prevTag, taggedWordArray[1]].sort());
          if (this.statisticsTagTagPair.has(tags)) {
            const initialCount = this.statisticsTagTagPair.get(tags);
            this.statisticsTagTagPair.set(tags, initialCount + 1);
          } else {
            this.statisticsTagTagPair.set(tags, 1);
          }
        }
        prevTag = taggedWordArray[1];
      } else {
        console.error('#1 Error while loading annotated text: unhandled token (has <> than 2 parts): ', taggedWordArray);
      }
    });
    this.annotatedText = text;
    this.wordArray = Array.from(this.wordMap);
    this.statisticsTagArray = Array.from(this.statisticsTag);
    this.statisticsWordTagArray = Array.from(this.statisticsWordTag);
    this.statisticsTagTagPairArray = Array.from(this.statisticsTagTagPair);
    this.last = sortType.SORT_TYPE_WORD;
    this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
  }

  onStatisticsFileParsed(text: string) {
    const statistics = JSON.parse(text);
    if (statistics.statisticsTag && statistics.statisticsWordTag && statistics.statisticsTagTagPair && statistics.tagsDefinitionArray) {
      this.statisticsTagArray = statistics.statisticsTag;
      this.statisticsWordTagArray = statistics.statisticsWordTag;
      this.statisticsTagTagPairArray = statistics.statisticsTagTagPair;
      this.tagsDefinitionArray = statistics.tagsDefinition;
      this.statisticsTag = new Map<string, number>(this.statisticsTagArray);
      this.statisticsWordTag = new Map<string, number>(this.statisticsWordTagArray);
      this.statisticsTagTagPair = new Map<string, number>(this.statisticsTagTagPairArray);
      this.tagsDefinition = this.tagsDefinitionArray.reduce(function (result, item) {
        result[item[0]] = item[1];
        return result;
      }, {});
      console.log('this.tagsDefinition = ', this.tagsDefinition);
    } else {
      console.error('Error #5: can not parse statistics, statistics = ', statistics);
    }
  }

  onDictionaryParsed(text: string) {
    const possibleWords = text.split(' ');
    possibleWords.forEach((possibleWord: string) => {
      const taggedWordArray = possibleWord.split('|');
      if (taggedWordArray.length === 5) {
        this.addWordToMap(
          taggedWordArray[0],
          +taggedWordArray[1],
          taggedWordArray[2],
          taggedWordArray[3],
          taggedWordArray[4],
        );
      } else {
        console.error('#2 Error while loading dictionary: unhandled token (has <> than 5 parts): ', taggedWordArray);
      }
    });
    this.wordArray = Array.from(this.wordMap);
    this.last = sortType.SORT_TYPE_WORD;
    this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
  }

  wordSortClick(): void {
    this.wordUp = !this.wordUp;
    if (this.last === sortType.SORT_TYPE_WORD) {
      this.wordArray = this.wordArray.reverse();
    } else {
      this.last = sortType.SORT_TYPE_WORD;
      this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
    }
  }

  countSortClick(): void {
    this.countUp = !this.countUp;
    if (this.last === sortType.SORT_TYPE_COUNT) {
      this.wordArray = this.wordArray.reverse();
    } else {
      this.last = sortType.SORT_TYPE_COUNT;
      this.wordArray = this.countUp ? this.wordArray.sort(compareCountDown) : this.wordArray.sort(compareCountUp);
    }
  }

  tagNameSortClick() {
    this.tagNameUp = !this.tagNameUp;
    this.statisticsTagArray = this.tagNameUp ? this.statisticsTagArray.sort((a, b) => {
      if (a[0] > b[0]) {
        return 1;
      }
      if (a[0] === b[0]) {
        return 0;
      }
      return -1;
    }) : this.statisticsTagArray.sort((a, b) => {
        if (a[0] > b[0]) {
          return -1;
        }
        if (a[0] === b[0]) {
          return 0;
        }
        return 1;
      }
    );
  }

  tagCountSortClick() {
    this.tagCountUp = !this.tagCountUp;
    this.statisticsTagArray = this.tagCountUp ? this.statisticsTagArray.sort((a, b) => a[1] - b[1]) :
      this.statisticsTagArray.sort((a, b) => b[1] - a[1]);
  }

  wordTagWordSortClick() {
    this.wordTagWordUp = !this.wordTagWordUp;
    this.statisticsWordTagArray = this.wordTagWordUp ? this.statisticsWordTagArray.sort((a, b) => {
      if (JSON.parse(a[0]).word > JSON.parse(b[0]).word) {
        return 1;
      }
      if (JSON.parse(a[0]).word === JSON.parse(b[0]).word) {
        return 0;
      }
      return -1;
    }) : this.statisticsWordTagArray.sort((a, b) => {
        if (JSON.parse(a[0]).word > JSON.parse(b[0]).word) {
          return -1;
        }
        if (JSON.parse(a[0]).word === JSON.parse(b[0]).word) {
          return 0;
        }
        return 1;
      }
    );
  }

  wordTagTagSortClick() {
    this.wordTagTagUp = !this.wordTagTagUp;
    this.statisticsWordTagArray = this.wordTagTagUp ? this.statisticsWordTagArray.sort((a, b) => {
      if (JSON.parse(a[0]).tag > JSON.parse(b[0]).tag) {
        return 1;
      }
      if (JSON.parse(a[0]).tag === JSON.parse(b[0]).tag) {
        return 0;
      }
      return -1;
    }) : this.statisticsWordTagArray.sort((a, b) => {
        if (JSON.parse(a[0]).tag > JSON.parse(b[0]).tag) {
          return -1;
        }
        if (JSON.parse(a[0]).tag === JSON.parse(b[0]).tag) {
          return 0;
        }
        return 1;
      }
    );
  }

  wordTagCountSortClick() {
    this.wordTagCountUp = !this.wordTagCountUp;
    this.statisticsWordTagArray = this.wordTagCountUp ? this.statisticsWordTagArray.sort((a, b) => a[1] - b[1]) :
      this.statisticsWordTagArray.sort((a, b) => b[1] - a[1]);
  }

  TagTagTagSortClick() {
    this.tagTagTagUp = !this.tagTagTagUp;
    this.statisticsTagTagPairArray = this.tagTagTagUp ? this.statisticsTagTagPairArray.sort((a, b) => {
      if (JSON.parse(a[0])[0] > JSON.parse(b[0])[0]) {
        return 1;
      }
      if (JSON.parse(a[0])[0] === JSON.parse(b[0])[0]) {
        return 0;
      }
      return -1;
    }) : this.statisticsTagTagPairArray.sort((a, b) => {
        if (JSON.parse(a[0])[0] > JSON.parse(b[0])[0]) {
          return -1;
        }
        if (JSON.parse(a[0])[0] === JSON.parse(b[0])[0]) {
          return 0;
        }
        return 1;
      }
    );
  }


  TagTagCountSortClick() {
    this.tagTagCountUp = !this.tagTagCountUp;
    this.statisticsTagTagPairArray = this.tagTagCountUp ? this.statisticsTagTagPairArray.sort((a, b) => a[1] - b[1]) :
      this.statisticsTagTagPairArray.sort((a, b) => b[1] - a[1]);
  }

  editItem(word): void {
    console.log('EDIT ITEM ', word);
    this.newWord = word;
    this.editWord = word;
    this.dialogOpenMode = DialogOpenMode.OPEN_MODE_WORD;
    this.dialogWindow.nativeElement.showModal();
  }

  editTag(word, tag): void {
    console.log('EDIT Tag for ', word);
    this.wordForTag = word;
    this.newWord = tag;
    this.editWord = tag;
    this.dialogOpenMode = DialogOpenMode.OPEN_MODE_TAG;
    this.dialogWindow.nativeElement.showModal();
  }

  editCanonForm(word, canonForm): void {
    console.log('EDIT CanonForm for ', word);
    this.wordForTag = word;
    this.newWord = canonForm;
    this.editWord = canonForm;
    this.dialogOpenMode = DialogOpenMode.OPEN_MODE_CANON_FORM;
    this.dialogWindow.nativeElement.showModal();
  }

  editCanonFormTags(word, canonFormTags): void {
    console.log('EDIT CanonFormTags for ', word);
    this.wordForTag = word;
    this.newWord = canonFormTags;
    this.editWord = canonFormTags;
    this.dialogOpenMode = DialogOpenMode.OPEN_MODE_CANON_FORM_TAGS;
    this.dialogWindow.nativeElement.showModal();
  }

  addNewWord(): void {
    this.newWord = 'word';
    this.editWord = '';
    this.dialogOpenMode = DialogOpenMode.OPEN_MODE_CREATE_WORD;
    this.dialogWindow.nativeElement.showModal();
  }

  saveWord(): void {
    const dialogOpenMode = this.dialogOpenMode;
    const wordForTag = this.wordForTag;
    const editWord = this.editWord;
    const newWord = this.newWord;
    this.dialogOpenMode = null;
    this.wordForTag = '';
    this.editWord = '';
    this.newWord = '';
    this.dialogWindow.nativeElement.close();
    if (newWord === editWord) {
      return;
    }
    switch (dialogOpenMode) {
      case DialogOpenMode.OPEN_MODE_WORD:
        if (this.wordMap.has(newWord)) {
          this.wordMap.set(newWord, {
            count: this.wordMap.get(newWord).count + this.wordMap.get(editWord).count,
            tags: this.wordMap.get(newWord).tags,
            canonForm: this.wordMap.get(newWord).canonForm
          });
          this.resetAfterWordChange(editWord, newWord);
        } else {
          this.wordMap.set(newWord, {
            count: this.wordMap.get(editWord).count,
            tags: this.getTagString(newWord),
            canonForm: this.getCanonFormString(newWord)
          });
          this.resetAfterWordChange(editWord, newWord);
        }
        break;
      case DialogOpenMode.OPEN_MODE_TAG:
        this.wordMap.set(wordForTag, {
          count: this.wordMap.get(wordForTag).count,
          tags: newWord,
          canonForm: this.wordMap.get(wordForTag).canonForm
        });
        const tagArray = newWord.split('/');
        tagArray.forEach(tag => {
          console.log('TAG: ', tag);
          console.log('this.tagsDefinition[tag]: ', this.tagsDefinition[tag]);
          if (this.tagsDefinition[tag] === undefined) {
            const timerId = setInterval(() => {
              if (this.dialogWindow.nativeElement.open) {
                console.log('dialog is open tag: ', tag);
              } else {
                console.log('dialog is closed, tag: ', tag);
                this.wordForTag = tag;
                this.newWord = 'Enter tha tag \"' + tag + '\" definition';
                this.editWord = '';
                this.dialogOpenMode = DialogOpenMode.OPEN_MODE_ADD_TAG_DEFINITION;
                this.dialogWindow.nativeElement.showModal();
                clearInterval(timerId);
              }
            }, 1000);
          }
        });
        break;
      case DialogOpenMode.OPEN_MODE_CANON_FORM:
        if (!this.canonFormMap.has(newWord)) {
          this.canonFormMap.set(newWord, this.getTagString(newWord).split('/').filter(tag => baseForms.indexOf(tag) !== -1).join('/'));
        }
        this.wordMap.set(wordForTag, {
          count: this.wordMap.get(wordForTag).count,
          tags: this.wordMap.get(wordForTag).tags,
          canonForm: newWord
        });
        break;
      case DialogOpenMode.OPEN_MODE_CREATE_WORD:
        if (this.wordMap.has(newWord)) {
          alert('В словаре уже есть слово = ' + newWord);
        } else {
          this.addWordToMap(newWord, 0);
        }
        break;
      case DialogOpenMode.OPEN_MODE_CANON_FORM_TAGS:
        this.canonFormMap.set(this.wordMap.get(wordForTag).canonForm, newWord);
        break;
      case DialogOpenMode.OPEN_MODE_ADD_TAG_DEFINITION:
        this.tagsDefinition[wordForTag] = newWord;
        this.tagsDefinitionArray = Object.entries(this.tagsDefinition).sort();
        break;
    }
    this.last = sortType.SORT_TYPE_WORD;
    this.wordArray = Array.from(this.wordMap);
    this.wordArray = this.wordUp ? this.wordArray.sort(compareWordDown) : this.wordArray.sort(compareWordUp);
  }

  private resetAfterWordChange(editWord: string, newWord: string) {
    this.annotatedText.replace(editWord, newWord);
    this.wordMap.delete(editWord);
  }

  saveDictionary() {
    let joinedText = '';
    this.wordMap.forEach((value: { count: number, tags: string, canonForm: string }, key: string) => {
      let textPart: string;
      textPart = key + '|' +
        value.count + '|' +
        value.tags + '|' +
        value.canonForm + '|' +
        this.canonFormMap.get(value.canonForm);
      joinedText = joinedText.concat(' ').concat(textPart);
    });
    const blob = new Blob([joinedText.trim()], {type: 'text/plain'});
    saveAs(blob, 'dictionary.txt');
  }

  saveAnnotatedText() {
    let blob;
    if (this.annotatedTextIsEdit) {
      blob = new Blob([this.safeAnnotatedText], {type: 'text/plain'});
    } else {
      blob = new Blob([this.annotatedText], {type: 'text/plain'});
    }
    saveAs(blob, 'annotatedText.txt');
  }

  saveStatistics() {
    let blob;
    const statistics = JSON.stringify({
      tagsDefinition: [...Object.entries(this.tagsDefinition)],
      statisticsTag: [...Array.from(this.statisticsTag)],
      statisticsWordTag: [...Array.from(this.statisticsWordTag)],
      statisticsTagTagPair: [...Array.from(this.statisticsTagTagPair)]
    });
    blob = new Blob([statistics], {type: 'text/plain'});
    saveAs(blob, 'statisticsFile.txt');
  }

  getTagString(word: string): string {
    const tags: string[] = lexicon[word.toLowerCase()] ? lexicon[word.toLowerCase()].split('|') : [];
    return tags.join('/');
  }

  getCanonFormString(word: string): string {
    const token = compendium.analyse(word)[0].tokens[0];
    let canonForm = '';
    if (token.attr.is_noun && token.attr.singular) {
      canonForm = token.attr.singular;
    }
    if (token.attr.is_verb && token.attr.infinitive) {
      canonForm = token.attr.infinitive;
    }
    if (!canonForm) {
      canonForm = token.norm;
    }
    return canonForm ? canonForm : '';
  }

  addWordToMap(word: string, count: number, tags?: string, canonForm?: string, canonFormTags?: string): void {
    if (!tags) {
      tags = this.getTagString(word);
    }
    if (canonForm && canonFormTags) {
      if (this.wordMap.has(word)) {
        const canonFormFromTable = this.wordMap.get(word).canonForm;
        if (canonFormFromTable !== canonForm) {
          const change = confirm('Вы хотите заменить каноническую форму слова \"' + word + '\" с \"' +
            canonFormFromTable + '\" на \"' + canonForm + '\"?');
          if (!change) {
            canonForm = canonFormFromTable;
          }
        } else {
          if (this.canonFormMap.has(canonForm)) {
            const canonFormTagsArray = canonFormTags.split('/');
            canonFormTags = canonFormTagsArray.concat(this.canonFormMap.get(canonForm).split('/').filter(form =>
              canonFormTagsArray.indexOf(form) === -1)).filter(tag => baseForms.indexOf(tag) !== -1).join('/');
          }
          this.canonFormMap.set(canonForm, canonFormTags);
        }
        const tagArray = tags.split('/');
        tags = tagArray.concat(this.wordMap.get(word).tags.split('/').filter(tag =>
          tagArray.indexOf(tag) === -1)).join('/');
        this.wordMap.set(word, {
          count: this.wordMap.get(word).count + count,
          tags: tags,
          canonForm: canonForm
        });
      } else {
        this.wordMap.set(word, {count: 1, tags: tags, canonForm: canonForm});
        if (this.canonFormMap.has(canonForm)) {
          const canonFormTagsArray = canonFormTags.split('/');
          canonFormTags = canonFormTagsArray.concat(this.canonFormMap.get(canonForm).split('/').filter(form =>
            canonFormTagsArray.indexOf(form) === -1)).filter(tag => baseForms.indexOf(tag) !== -1).join('/');
        }
        this.canonFormMap.set(canonForm, canonFormTags);
      }
    } else {
      let initialCount = 0;
      canonForm = this.getCanonFormString(word);
      if (this.wordMap.has(word)) {
        initialCount = this.wordMap.get(word).count;
        const tagArray = tags.split('/');
        tags = tagArray.concat(this.wordMap.get(word).tags.split('/').filter(tag =>
          tagArray.indexOf(tag) === -1)).join('/');
      } else {
        this.tagsDefinitionArray.forEach(tag => {
          const wordAndTag = JSON.stringify({word: word.toLowerCase(), tag: tag[0]});
          this.statisticsWordTag.set(wordAndTag, 0);
        });
      }
      this.wordMap.set(word, {
        count: initialCount + count,
        tags: tags,
        canonForm: canonForm
      });
      if (!this.canonFormMap.has(canonForm)) {
        this.canonFormMap.set(canonForm, this.getTagString(canonForm).split('/').filter(tag => baseForms.indexOf(tag) !== -1).join('/'));
      }
    }
  }

  onShowTooltipClick(): void {
    this.isTooltipVisible = !this.isTooltipVisible;
  }

  onTagSearchChange() {
    if (this.tagSearch) {
      this.tagsDefinitionArray = Object.entries(this.tagsDefinition).filter(pair =>
        pair[0].toLowerCase().indexOf(this.tagSearch.toLowerCase()) !== -1);
    } else {
      this.tagsDefinitionArray = Object.entries(this.tagsDefinition);
    }
  }

  onWordSearchChange() {
    if (this.wordSearch) {
      this.tagsDefinitionArray = Object.entries(this.tagsDefinition).filter(pair => {
        const tag: string = pair[1];
        return tag.toLowerCase().indexOf(this.wordSearch.toLowerCase()) !== -1;
      });
    } else {
      this.tagsDefinitionArray = Object.entries(this.tagsDefinition);
    }
  }

  analyzeAnnotatedText() {
    this.annotatedTextComponent.nativeElement.setAttribute('readonly', 'readonly');
    const possibleWords = this.annotatedText.split(' ');
    let stopParse = false;
    possibleWords.forEach((possibleWord: string) => {
      const taggedWordArray = possibleWord.split('_');
      if (taggedWordArray.length === 2 && taggedWordArray[0] && taggedWordArray[1]) {
        if (this.tagger.tagSentence(taggedWordArray[0])[0].tag === 'word') {
          this.addWordToMap(
            taggedWordArray[0],
            1,
            taggedWordArray[1]);
        }
        if (this.tagsDefinition[taggedWordArray[1]] === undefined) {
          const timerId = setInterval(() => {
            if (this.dialogWindow.nativeElement.open) {
              console.log('dialog is open tag: ', taggedWordArray[1]);
            } else {
              console.log('dialog is closed, tag: ', taggedWordArray[1]);
              this.wordForTag = taggedWordArray[1];
              this.newWord = 'Enter tha tag \"' + taggedWordArray[1] + '\" definition';
              this.editWord = '';
              this.dialogOpenMode = DialogOpenMode.OPEN_MODE_ADD_TAG_DEFINITION;
              this.dialogWindow.nativeElement.showModal();
              clearInterval(timerId);
            }
          }, 1000);
        }
        if (this.statisticsTag.has(taggedWordArray[1])) {
          const initialCount = this.statisticsTag.get(taggedWordArray[1]);
          this.statisticsTag.set(taggedWordArray[1], initialCount + 1);
        } else {
          this.statisticsTag.set(taggedWordArray[1], 1);
        }
      } else if (possibleWord) {
        alert('Аннотированный текст невалиден, проблемы с тегом: ' + possibleWord);
        this.annotatedTextComponent.nativeElement.removeAttribute('readonly');
        this.annotatedTextComponent.nativeElement.setAttribute('focused', 'focused');
        this.annotatedTextComponent.nativeElement.setAttribute('invalid', 'true');
        console.error('#3 Error while validating annotated text: unhandled token (has <> than 2 parts): ', taggedWordArray);
        stopParse = true;
        this.annotatedTextWithError = true;
      }
    });
    if (!stopParse) {
      if (this.annotatedTextComponent.nativeElement.getAttribute('invalid')) {
        this.annotatedTextComponent.nativeElement.removeAttribute('invalid');
      }
      this.annotatedTextWithError = false;
      this.annotatedTextIsEdit = false;
      this.wordArray = Array.from(this.wordMap);
    }
  }

  onAnnotatedTextKeydown(event) {
    if (event.keyCode === 13) {
      this.analyzeAnnotatedText();
    }
  }

  onAnnotatedTextDblclick() {
    if (!this.annotatedTextIsEdit) {
      this.safeAnnotatedText = this.annotatedText;
      this.annotatedTextComponent.nativeElement.removeAttribute('readonly');
      this.annotatedTextComponent.nativeElement.setAttribute('focused', 'focused');
      this.annotatedTextIsEdit = true;
    }
  }

  cancelAnnotatedTextChange() {
    this.annotatedTextWithError = false;
    this.annotatedTextIsEdit = false;
    this.annotatedText = this.safeAnnotatedText;
    this.annotatedTextComponent.nativeElement.setAttribute('readonly', 'readonly');
    if (this.annotatedTextComponent.nativeElement.getAttribute('invalid')) {
      this.annotatedTextComponent.nativeElement.removeAttribute('invalid');
    }
  }
}

function compareWordUp(a, b) {
  if (a[0] < b[0]) {
    return -1;
  }
  if (a[0] > b[0]) {
    return 1;
  }
  return 0;
}

function compareWordDown(a, b) {
  if (a[0] > b[0]) {
    return -1;
  }
  if (a[0] < b[0]) {
    return 1;
  }
  return 0;
}

function compareCountUp(a, b) {
  if (a[1].count < b[1].count) {
    return -1;
  }
  if (a[1].count > b[1].count) {
    return 1;
  }
  return 0;
}

function compareCountDown(a, b) {
  if (a[1].count < b[1].count) {
    return -1;
  }
  if (a[1].count > b[1].count) {
    return 1;
  }
  return 0;
}
